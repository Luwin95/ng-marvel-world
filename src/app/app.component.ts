import { Component, OnInit, OnDestroy } from '@angular/core';
import { AvengerService } from './services/avenger/avenger.service';
import { Avenger } from './shared/models/avenger';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'marvel-world';
  avengers: Avenger[];
  avengersSubscription: Subscription;

  constructor(private avengerService: AvengerService) {}

  ngOnInit() {
    this.avengersSubscription = this.avengerService.avengersSubject.subscribe(
      (avengers) =>{
        this.avengers = avengers;
      }
    );
    this.avengerService.emitAvengersSubject();
  }

  ngOnDestroy() {
    this.avengerService.avengersSubject.unsubscribe();
  }
}
