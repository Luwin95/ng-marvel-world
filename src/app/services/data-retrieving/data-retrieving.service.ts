import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Avenger } from 'src/app/shared/models/avenger';
import { HttpClient } from '@angular/common/http';
import { AVENGERS_URL } from 'src/app/shared/constants/url-constants';

@Injectable({
  providedIn: 'root'
})
export class DataRetrievingService {

  constructor(private http: HttpClient) { }

  getAvengersList(): Observable<Avenger[]>{
    return this.http.get<Avenger[]>(AVENGERS_URL);
  }
}
