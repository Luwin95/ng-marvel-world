import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Avenger } from '../../shared/models/avenger';
import { DataRetrievingService } from '../data-retrieving/data-retrieving.service';
import * as _ from 'lodash';
import { LikesStore } from 'src/app/shared/models/likes-store';

@Injectable({
  providedIn: 'root'
})
export class AvengerService {

  avengersSubject = new Subject<Avenger[]>();

  private maxLikes: number;

  private avengers: Avenger[];

  private likesStore: {[key: number]: number}  = {};

  constructor(private dataRetrievingService: DataRetrievingService) {}

  /**
   * Emit new value of avengers table
   */
  emitAvengersSubject() {
    if (!_.isNil(this.avengers)) {
      // If table has already been initialize emit value
      this.avengersSubject.next(this.avengers.slice());
    } else {
      // If table has not been initialized 
      // Retrieve datas
      this.dataRetrievingService.getAvengersList().subscribe(avengersResponse =>{
        this.avengers = avengersResponse;
        if (Object.keys(this.likesStore).length === 0) {
          // Initialize likesStore
          this.initLikesStore();
        } else {
          // Reinitialize likesStore case of cleared searchBar
          this.reinitLikesStore();
        }
        // Initialize maxLikes
        this.getMaxLikes();
        // Emit value
        this.avengersSubject.next(this.avengers.slice());
      });
    }
  }

  /**
   * Increment avenger fan index
   * @param avengerIndex index in table of avenger
   */
  addNewFan(avengerIndex: number) {
    // Increment value in avengers table
    this.avengers[avengerIndex].likes += 1;
    // Update value of maxLikes and isBaddass booleans
    this.getMaxLikes();
    // Update value in likesStore
    this.likesStore[this.avengers[avengerIndex].id] += 1;
    this.emitAvengersSubject();
  }

  /**
   * Filter avengers table searching for string param in skills and name attributes
   * @param searchString String enterred by user
   */
  searchAvenger(searchString: string) {
    // Retrieve all datas before filtering
    this.dataRetrievingService.getAvengersList().subscribe(avengersResponse =>{
      this.avengers = avengersResponse;
      // If given string is not empty
      if (searchString !== '') {
        // Filter table
        const avengersFiltered = this.avengers.filter(
          avenger => this.checkAvenger(avenger, searchString)
        );
        this.avengers = avengersFiltered;
        // If any avenger is returned reinit likeStore and update maxLikes and isBaddass booleans
        if (this.avengers.length > 0) {
          this.reinitLikesStore();
          this.getMaxLikes();
        }
      } else {
        this.avengers = null;
      }
      this.emitAvengersSubject();
    });
  }

  /**
   * Check if an avenger got a skill or a name matching a given string
   * @param avenger The avenger to check
   * @param searchString The string to check
   */
  checkAvenger(avenger: Avenger, searchString: string){
    let exist = false;
    // Check Name
    if (avenger.name.toLowerCase().includes(searchString.toLowerCase())) {
      exist = true;
    }
    // Check Skill
    for (const skill of avenger.skills) {
      if (skill.toLowerCase().includes(searchString.toLowerCase())) {
        exist = true;
      }
    }
    return exist;
  }

  /**
   * Get The max of all likes attribute and update isBaddass attribute of all avengers
   */
  getMaxLikes() {

    // Maxlikes will take the value of the avenger likes attribute that is maximal
    this.maxLikes = this.avengers.reduce( (prev, current) => {
        return (prev.likes > current.likes) ? prev : current;
    }).likes;

    // Update all avengers isBaddass attribute :
    // - it will be true if it's equal to the maxLikes of array
    // - it will be false otherwise
    for (const avenger of this.avengers) {
      avenger.isBaddass = avenger.likes === this.maxLikes;
    }
  }

  /**
   * Stock all the value of the avenger likes into likesStore Object
   * Is only called at first initialization of avengers table
   */
  initLikesStore() {
    for (const avenger of this.avengers) {
      this.likesStore[avenger.id] = avenger.likes;
    }
  }

  /**
   * Reinit all avengers likes attribute with the value stored in likesStore
   */
  reinitLikesStore() {
    for (const avenger of this.avengers) {
      avenger.likes = this.likesStore[avenger.id];
    }
  }
}
