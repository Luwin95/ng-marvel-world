import { Component, OnInit, Input } from '@angular/core';
import { Avenger } from '../../models/avenger';
import { faStar, faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { AvengerService } from 'src/app/services/avenger/avenger.service';

@Component({
  selector: 'app-avenger-card',
  templateUrl: './avenger-card.component.html',
  styleUrls: ['./avenger-card.component.css']
})
export class AvengerCardComponent implements OnInit {

  @Input() avenger: Avenger;
  @Input() cardIndex: number;

  faStar = faStar;
  faThumbsUp = faThumbsUp;

  constructor(private avengerService: AvengerService) { }

  ngOnInit() {
  }

  newFan() {
    this.avengerService.addNewFan(this.cardIndex);
  }

}
