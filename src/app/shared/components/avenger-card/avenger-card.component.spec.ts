import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvengerCardComponent } from './avenger-card.component';

describe('AvengerCardComponent', () => {
  let component: AvengerCardComponent;
  let fixture: ComponentFixture<AvengerCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvengerCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvengerCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
