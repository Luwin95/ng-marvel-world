import { Component, OnInit } from '@angular/core';
import { faBroom } from '@fortawesome/free-solid-svg-icons';
import { AvengerService } from 'src/app/services/avenger/avenger.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  searchString: string;

  btnIsDisabled: string;

  faBroom = faBroom;

  constructor(private avengerService: AvengerService) { }

  ngOnInit() {
    this.searchString = '';
    this.btnIsDisabled = 'disabled';
  }

  clearSearchBar() {
    this.searchString = '';
    this.avengerService.searchAvenger(this.searchString);
  }

  searchAvengers() {
    this.avengerService.searchAvenger(this.searchString);
  }
}
