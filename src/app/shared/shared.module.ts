import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { AvengerCardComponent } from './components/avenger-card/avenger-card.component';



@NgModule({
  declarations: [NavbarComponent, AvengerCardComponent],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule
  ],
  exports: [NavbarComponent, AvengerCardComponent]
})
export class SharedModule { }
