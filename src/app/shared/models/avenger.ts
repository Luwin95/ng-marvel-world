export class Avenger{
    id?: number;
    name?: string;
    skills?: string[];
    picture?: string;
    likes?: number;
    isBaddass?: boolean;
}
