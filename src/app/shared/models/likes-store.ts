export class LikesStore{
    id?: number;
    likes?: number;

    constructor(pId: number, pLikes: number){
        this.id = pId,
        this.likes = pLikes;
    }
}